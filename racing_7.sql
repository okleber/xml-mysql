-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 22, 2017 at 09:13 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `racing`
--

-- --------------------------------------------------------

--
-- Table structure for table `meetings`
--

CREATE TABLE `meetings` (
  `meeting_id` int(11) NOT NULL,
  `betslip_type` varchar(1000) DEFAULT NULL,
  `code` varchar(1000) DEFAULT NULL,
  `country` varchar(1000) DEFAULT NULL,
  `date` date NOT NULL,
  `name` varchar(1000) DEFAULT NULL,
  `number` int(255) NOT NULL,
  `nz` varchar(10000) DEFAULT '1',
  `penetrometer` varchar(1000) DEFAULT NULL,
  `status` varchar(1000) DEFAULT NULL,
  `track_dir` varchar(1000) DEFAULT NULL,
  `type` varchar(1000) DEFAULT NULL,
  `venue` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pools`
--

CREATE TABLE `pools` (
  `pool_id` int(11) NOT NULL,
  `race_id` int(11) NOT NULL,
  `type` varchar(1000) DEFAULT NULL,
  `number` varchar(1000) DEFAULT NULL,
  `name` varchar(1000) DEFAULT NULL,
  `amount` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pool_odds`
--

CREATE TABLE `pool_odds` (
  `pool_odd_id` int(11) NOT NULL,
  `race_id` int(11) NOT NULL,
  `bet_type` varchar(1000) NOT NULL,
  `comingled` varchar(1000) NOT NULL,
  `comingled_info` varchar(1000) NOT NULL,
  `comingled_brand` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `races`
--

CREATE TABLE `races` (
  `race_id` int(11) NOT NULL,
  `meeting_id` int(11) NOT NULL,
  `class` varchar(1000) DEFAULT NULL,
  `length` int(255) DEFAULT NULL,
  `name` varchar(1000) NOT NULL,
  `norm_time` datetime DEFAULT NULL,
  `number` int(255) NOT NULL,
  `overseas_number` int(255) DEFAULT NULL,
  `stake` int(255) DEFAULT NULL,
  `status` varchar(1000) DEFAULT NULL,
  `track` varchar(1000) DEFAULT NULL,
  `venue` varchar(1000) DEFAULT NULL,
  `weather` varchar(1000) DEFAULT NULL,
  `result_status` varchar(1000) DEFAULT NULL,
  `winnerstime` varchar(1000) DEFAULT NULL,
  `winnerstrainer` varchar(1000) DEFAULT NULL,
  `winnersbreeding` varchar(1000) DEFAULT NULL,
  `winnersowner` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `race_entries`
--

CREATE TABLE `race_entries` (
  `race_entry_id` int(11) NOT NULL,
  `race_id` int(11) NOT NULL,
  `barrier` int(255) DEFAULT NULL,
  `jockey` varchar(1000) DEFAULT NULL,
  `jockey_allowance` varchar(1000) DEFAULT NULL,
  `jockey_weight` varchar(1000) DEFAULT NULL,
  `name` varchar(1000) NOT NULL,
  `number` int(255) NOT NULL,
  `scratched` int(255) NOT NULL DEFAULT '0',
  `scr` int(255) DEFAULT NULL,
  `win` varchar(1000) DEFAULT NULL,
  `plc` varchar(1000) DEFAULT NULL,
  `rank` int(255) DEFAULT NULL,
  `favouritism` varchar(1000) DEFAULT NULL,
  `margin` varchar(1000) DEFAULT NULL,
  `distance` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `race_options`
--

CREATE TABLE `race_options` (
  `race_option_id` int(11) NOT NULL,
  `race_id` int(11) NOT NULL,
  `number` int(255) DEFAULT NULL,
  `type` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `meetings`
--
ALTER TABLE `meetings`
  ADD PRIMARY KEY (`meeting_id`);

--
-- Indexes for table `pools`
--
ALTER TABLE `pools`
  ADD PRIMARY KEY (`pool_id`),
  ADD KEY `race_id` (`race_id`);

--
-- Indexes for table `pool_odds`
--
ALTER TABLE `pool_odds`
  ADD PRIMARY KEY (`pool_odd_id`),
  ADD KEY `race_id` (`race_id`);

--
-- Indexes for table `races`
--
ALTER TABLE `races`
  ADD PRIMARY KEY (`race_id`),
  ADD KEY `meeting_id` (`meeting_id`);

--
-- Indexes for table `race_entries`
--
ALTER TABLE `race_entries`
  ADD PRIMARY KEY (`race_entry_id`),
  ADD KEY `race_id` (`race_id`);

--
-- Indexes for table `race_options`
--
ALTER TABLE `race_options`
  ADD PRIMARY KEY (`race_option_id`),
  ADD KEY `race_id` (`race_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `meetings`
--
ALTER TABLE `meetings`
  MODIFY `meeting_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pools`
--
ALTER TABLE `pools`
  MODIFY `pool_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pool_odds`
--
ALTER TABLE `pool_odds`
  MODIFY `pool_odd_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `races`
--
ALTER TABLE `races`
  MODIFY `race_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `race_entries`
--
ALTER TABLE `race_entries`
  MODIFY `race_entry_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `race_options`
--
ALTER TABLE `race_options`
  MODIFY `race_option_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `pools`
--
ALTER TABLE `pools`
  ADD CONSTRAINT `pools_ibfk_1` FOREIGN KEY (`race_id`) REFERENCES `races` (`race_id`);

--
-- Constraints for table `pool_odds`
--
ALTER TABLE `pool_odds`
  ADD CONSTRAINT `pool_odds_ibfk_1` FOREIGN KEY (`race_id`) REFERENCES `races` (`race_id`);

--
-- Constraints for table `races`
--
ALTER TABLE `races`
  ADD CONSTRAINT `races_ibfk_1` FOREIGN KEY (`meeting_id`) REFERENCES `meetings` (`meeting_id`);

--
-- Constraints for table `race_entries`
--
ALTER TABLE `race_entries`
  ADD CONSTRAINT `race_entries_ibfk_1` FOREIGN KEY (`race_id`) REFERENCES `races` (`race_id`);

--
-- Constraints for table `race_options`
--
ALTER TABLE `race_options`
  ADD CONSTRAINT `race_options_ibfk_1` FOREIGN KEY (`race_id`) REFERENCES `races` (`race_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
