-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 13, 2017 at 11:32 AM
-- Server version: 5.5.22
-- PHP Version: 5.3.10-1ubuntu3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `racing`
--

-- --------------------------------------------------------

--
-- Table structure for table `pools_pool_entries`
--

CREATE TABLE IF NOT EXISTS `pools_pool_entries` (
  `number` varchar(1000) NOT NULL,
  `scratched` varchar(1000) NOT NULL,
  `value` varchar(1000) DEFAULT NULL,
  `odds` varchar(1000) DEFAULT NULL,
  `favouritism` varchar(1000) DEFAULT NULL,
  `pool_id` int(11) NOT NULL,
  `entry_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`entry_id`),
  KEY `pools_pool_id` (`pool_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `pools_pool_entries`
--
ALTER TABLE `pools_pool_entries`
  ADD CONSTRAINT `pools_pool_entries_ibfk_2` FOREIGN KEY (`pool_id`) REFERENCES `pools_pool` (`pool_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
