#!/usr/bin/env python

#import MySQLdb
import xml.etree.ElementTree as ET
import datetime
import time
from urllib.request import urlopen
import logging
import mysql.connector


# from memory_profiler import profile

####Variables#####
loglevel = 'info' #info or debug

results_l = list()  # Store list of Objects

daybefore = datetime.datetime.utcfromtimestamp(time.time()) - datetime.timedelta(days=1)
date = daybefore.strftime('%Y-%m-%d')

db_username = 'test'
db_password = 'test'
db_host = '127.0.0.1'
db_port = 49162
db_database = 'racing'

base_url = 'http://xml.tab.co.nz/'
url_schedule = base_url + 'schedule/' + date
url_odds = base_url + 'odds/' + date
url_results = base_url + 'results/' + date
url_pools = base_url + 'pools/' + date


# url_schedule = 'schedule.xml'
# url_odds = 'odds.xml'
# url_results = 'results.xml'
# url_pools = 'pools.xml'


##################

def setNoneOrElementText(x):
    '''

    :param x: Element
    :return: str()  or some text
    '''
    if x is not None:
        return x.text
    else:
        return None


def etreeAttribute(tree, attr):
    try:
        return tree.attrib[attr]
    except:
        return None


class Db():
    username = str()
    password = str()
    host = str()
    port = int()
    database = str()
    cursor = None
    con = None

    def __init__(self):
        self.username = db_username
        self.password = db_password
        self.host = db_host
        self.port = db_port
        self.database = db_database
        self.connect()

    def query(self, statement, tupla):
        try:
            self.cursor.execute(statement, tupla)
            return 0
        except Exception as e: 
            logging.error(statement)
            logging.error(tupla)
            logging.error(e)
            return None

    def connect(self):
        logging.info("Connecting to database")
        #self.con = MySQLdb.connect(passwd=self.password, db=self.database, port=self.port, host=self.host, user=self.username)
        self.con = mysql.connector.connect(password=self.password, database=self.database, port=self.port, host=self.host,user=self.username)
        self.cursor = self.con.cursor()

    def disconnect(self):
        self.cursor.close()
        self.con.close()

    def commit(self):
        self.con.commit()


class Schedule():
    date = str()

    def set(self, tree):
        self.date = tree.attrib['date']
        return 0


class ScheduleMeeting():
    number = int()
    betslip_type = str()
    code = str()
    country = str()
    name = str()
    nz = str()
    penetrometer = str()
    status = str()
    track_dir = str()
    type = str()
    venue = str()
    parentSchedule = Schedule()
    sqlid = int()

    def __init__(self):
        self.sqlid = None

    def set(self, tree, P):
        self.number = tree.find('number').text
        self.date = tree.find('date').text
        self.betslip_type = setNoneOrElementText(tree.find('betslip_type'))
        self.code = setNoneOrElementText(tree.find('code'))
        self.country = setNoneOrElementText(tree.find('country'))
        self.name = setNoneOrElementText(tree.find('name'))
        self.nz = setNoneOrElementText(tree.find('nz'))
        self.penetrometer = setNoneOrElementText(tree.find('penetrometer'))
        self.status = setNoneOrElementText(tree.find('status'))
        self.track_dir = setNoneOrElementText(tree.find('track_dir'))
        self.type = setNoneOrElementText(tree.find('type'))
        self.venue = setNoneOrElementText(tree.find('venue'))
        self.parentSchedule = P

        return 0


class ScheduleRace():
    number = int()
    classs = str()
    length = int()
    name = str()
    norm_time = str()
    overseas_number = int()
    stake = int()
    status = str()
    track = str()
    venue = str()
    weather = str()
    options = str()
    parentMeeting = ScheduleMeeting()
    sqlid = int()

    def __init__(self):
        self.sqlid = None

    def set(self, tree, P):
        self.number = tree.find('number').text
        self.name = tree.find('name').text
        self.classs = setNoneOrElementText(tree.find('class'))
        self.length = setNoneOrElementText(tree.find('length'))
        self.name = setNoneOrElementText(tree.find('name'))
        self.norm_time = setNoneOrElementText(tree.find('norm_time'))
        self.overseas_number = setNoneOrElementText(tree.find('overseas_number'))
        self.stake = setNoneOrElementText(tree.find('stake'))
        self.status = setNoneOrElementText(tree.find('status'))
        self.track = setNoneOrElementText(tree.find('track'))
        self.venue = setNoneOrElementText(tree.find('venue'))
        self.weather = setNoneOrElementText(tree.find('weather'))
        self.options = setNoneOrElementText(tree.find('options'))
        self.parentMeeting = P

        return 0


class ResultRace():
    number = int()
    name = str()
    classs = str()
    stake = str()
    distance = str()
    status = str()
    winnerstrainer = str()
    winnersbreeding = str()
    winnersowner = str()
    winnerstime = str()
    parentScheduleRace = ScheduleRace()
    sqlid = int()

    def __init__(self):
        self.sqlid = None

    def set(self, tree, P):
        self.number = etreeAttribute(tree, 'number')
        self.name = etreeAttribute(tree, 'name')
        self.classs = etreeAttribute(tree, 'class')
        self.stake = etreeAttribute(tree, 'stake')
        self.distance = etreeAttribute(tree, 'distance')
        self.status = etreeAttribute(tree, 'status')
        self.winnerstrainer = etreeAttribute(tree, 'winnerstrainer')
        self.winnersbreeding = etreeAttribute(tree, 'winnersbreeding')
        self.winnersowner = etreeAttribute(tree, 'winnersowner')
        self.winnerstime = etreeAttribute(tree, 'winnerstime')
        self.parentScheduleRace = P


class ScheduleEntry():
    number = str()
    barrier = str()
    jockey = str()
    jockey_allowance = str()
    jockey_weight = str()
    handicap = str()
    scratched = str()
    name = str()
    parentRace = ScheduleRace()
    sqlid = int()

    def __init__(self):
        self.sqlid = None

    def set(self, tree, P):
        self.number = tree.find('number').text
        self.name = tree.find('name').text
        self.scratched = tree.find('scratched').text
        self.barrier = setNoneOrElementText(tree.find('barrier'))
        self.jockey = setNoneOrElementText(tree.find('jockey'))
        self.jockey_allowance = setNoneOrElementText(tree.find('jockey_allowance'))
        self.jockey_weight = setNoneOrElementText(tree.find('jockey_weight'))
        self.handicap = setNoneOrElementText(tree.find('handicap'))
        self.scratched = setNoneOrElementText(tree.find('scratched'))
        self.parentRace = P


class ScheduleOptions():
    number = str()
    type = str()
    parentRace = ScheduleRace()
    sqlid = int()

    def __init__(self):
        self.sqlid = None

    def set(self, tree, P):
        self.number = tree.find('number').text
        self.type = tree.find('type').text


class PoolsPool():
    bet_type = str()
    total = str()
    commingled = str()
    commingled_brand = str()
    commingled_info = str()
    parentRace = ScheduleRace()
    sqlid = int()

    def __init__(self):
        self.sqlid = None

    def set(self, tree, P):
        self.bet_type = tree.find('bet_type').text
        self.total = setNoneOrElementText(tree.find('total'))
        self.commingled = setNoneOrElementText(tree.find('commingled'))
        self.commingled_brand = setNoneOrElementText(tree.find('commingled_brand'))
        self.commingled_info = setNoneOrElementText(tree.find('commingled_info'))

        self.parentRace = P
        return 0


class PoolsLeg():
    number = str()
    meet = str()
    race = str()
    parentPool = PoolsPool()

    def __init__(self):
        self.sqlid = None

    def set(self, tree, P):
        self.number = tree.find('number').text
        self.meet = tree.find('meet').text
        self.race = tree.find('race').text
        self.parentPool = P
        return 0


class PoolsEntry():
    number = str()
    scratched = str()
    value = str()
    odds = str()
    favouritism = str()
    parentPool = PoolsPool()

    def __init__(self):
        self.sqlid = None

    def set(self, tree, P):
        self.number = tree.find('number').text
        self.scratched = tree.find('scratched').text
        self.value = setNoneOrElementText(tree.find('value'))
        self.odds = setNoneOrElementText(tree.find('odds'))
        self.favouritism = setNoneOrElementText(tree.find('favouritism'))

        self.parentPool = P
        return 0


class ResultPlacing():
    number = int()
    name = str()
    rank = str()
    jockey = str()
    favouritism = str()
    distance = str()
    margin = str()
    parentResultRace = ResultRace()
    sqlid = int()

    def __init__(self):
        self.sqlid = None

    def set(self, tree, P):
        self.number = etreeAttribute(tree, 'number')
        self.name = etreeAttribute(tree, 'name')
        self.rank = etreeAttribute(tree, 'rank')
        self.jockey = etreeAttribute(tree, 'jockey')
        self.favouritism = etreeAttribute(tree, 'favouritism')
        self.distance = etreeAttribute(tree, 'distance')
        self.margin = etreeAttribute(tree, 'margin')
        self.parentResultRace = P


class ResultRunner():
    number = int()
    name = str()
    jockey = str()
    distance = str()
    finish_position = str()
    parentResultRace = ResultRace()
    sqlid = int()

    def __init__(self):
        self.sqlid = None

    def set(self, tree, P):
        self.number = etreeAttribute(tree, 'number')
        self.name = etreeAttribute(tree, 'name')
        self.jockey = etreeAttribute(tree, 'jockey')
        self.distance = etreeAttribute(tree, 'distance')
        self.finish_position = etreeAttribute(tree, 'finish_position')
        self.parentResultRace = P


class ResultScratching():
    number = int()
    name = str()
    classs = str()
    distance = str()
    stake = str()
    parentResultRace = ResultRace()
    sqlid = int()

    def __init__(self):
        self.sqlid = None

    def set(self, tree, P):
        self.number = tree.attrib['number']
        self.name = etreeAttribute(tree, 'name')
        self.classs = etreeAttribute(tree, 'class')
        self.distance = etreeAttribute(tree, 'distance')
        self.stake = etreeAttribute(tree, 'stake')
        self.parentResultRace = P


class ResultPool():
    number = int()
    name = str()
    type = str()
    amount = str()
    parentResultRace = ResultRace()
    sqlid = int()

    def __init__(self):
        self.sqlid = None

    def set(self, tree, P):
        self.number = tree.attrib['number']
        self.name = etreeAttribute(tree, 'name')
        self.type = etreeAttribute(tree, 'type')
        self.amount = etreeAttribute(tree, 'amount')
        self.parentResultRace = P


class OddsRace():
    number = int()
    winpool = str()
    plcpool = str()
    qlapool = str()
    tfapool = str()
    parentScheduleRace = ScheduleRace()
    sqlid = int()

    def __init__(self):
        self.sqlid = None

    def set(self, tree, P):
        self.number = etreeAttribute(tree, 'number')
        self.winpool = etreeAttribute(tree, 'winpool')
        self.plcpool = etreeAttribute(tree, 'plcpool')
        self.qlapool = etreeAttribute(tree, 'qlapool')
        self.tfapool = etreeAttribute(tree, 'tfapool')
        self.parentScheduleRace = P


class OddsEntry():
    number = int()
    scr = str()
    win = str()
    plc = str()
    parentOddsRace = OddsRace()
    sqlid = int()

    def __init__(self):
        self.sqlid = None

    def set(self, tree, P):
        self.number = etreeAttribute(tree, 'number')
        self.scr = etreeAttribute(tree, 'scr')
        self.win = etreeAttribute(tree, 'win')
        self.plc = etreeAttribute(tree, 'plc')
        self.parentOddsRace = P


class OddsBetType():
    bet_type = str()
    comingled = str()
    comingled_info = str()
    comingled_brand = str()
    parentOddsRace = OddsRace()
    sqlid = int()

    def __init__(self):
        self.sqlid = None

    def set(self, tree, P):
        self.bet_type = setNoneOrElementText(tree)
        self.comingled = etreeAttribute(tree, 'comingled')
        self.comingled_info = etreeAttribute(tree, 'comingled_info')
        self.comingled_brand = etreeAttribute(tree, 'comingled_brand')
        self.parentOddsRace = P


def open_xml_url(url):
    content = urlopen(url).read()
    # content = open(url).read()
    tree = ET.fromstring(content)
    return tree


# @profile
def parse_xml(tree, type):
    '''
    :param tree: xml element tree
    :param xml type [schedule, pools, odds, results ]
    :return: 0 or None
    '''
    if type == 'schedule':
        SC = Schedule()
        SC.set(tree)
        for meeting in tree.findall('meetings/meeting'):
            MM = ScheduleMeeting()
            MM.set(meeting, SC)
            results_l.append(MM)
            for race in meeting.findall('races/race'):
                RA = ScheduleRace()
                RA.set(race, MM)
                results_l.append(RA)
                for option in race.findall('options/option'):
                    OP = ScheduleOptions()
                    OP.set(option, RA)
                    results_l.append(OP)
                for entry in race.findall('entries/entry'):
                    EN = ScheduleEntry()
                    EN.set(entry, RA)
                    results_l.append(EN)

    elif type == 'results':
        mdate = tree.attrib['date']
        for meeting in tree.findall('meeting'):
            mnumber = meeting.attrib['number']
            for race in meeting.findall('races/race'):
                rnumber = race.attrib['number']
                ###Locating the parent Race
                for item in results_l:
                    if isinstance(item, ScheduleRace):
                        if item.number == rnumber:
                            if item.parentMeeting.number == mnumber:
                                if item.parentMeeting.date == mdate:
                                    RA = ResultRace()
                                    RA.set(race, item)
                                    results_l.append(RA)
                                    break

                for placing in race.findall('placings/placing'):
                    PL = ResultPlacing()
                    PL.set(placing, RA)
                    results_l.append(PL)
                for runner in race.findall('also_ran/runners/runner'):
                    RU = ResultRunner()
                    RU.set(runner, RA)
                    results_l.append(RU)
                for pool in race.findall('pools/pool'):
                    PO = ResultPool()
                    PO.set(pool, RA)
                    results_l.append(PO)
                for scratching in race.findall('scratchings/scratching'):
                    SC = ResultScratching()
                    SC.set(scratching, RA)
                    results_l.append(SC)

    elif type == 'odds':
        mdate = tree.attrib['date']
        for meeting in tree.findall('meeting'):
            mnumber = meeting.attrib['number']
            for race in meeting.findall('races/race'):
                rnumber = race.attrib['number']
                ###Locating the parent Race
                for item in results_l:
                    if isinstance(item, ScheduleRace):
                        if item.number == rnumber:
                            if item.parentMeeting.number == mnumber:
                                if item.parentMeeting.date == mdate:
                                    OR = OddsRace()
                                    OR.set(race, item)
                                    results_l.append(OR)
                                    break

                for bet_type in race.findall('pools/pool_odds/bet_type'):
                    OB = OddsBetType()
                    OB.set(bet_type, OR)
                    results_l.append(OB)
                for entry in race.findall('entries/entry'):
                    OE = OddsEntry()
                    OE.set(entry, OR)
                    results_l.append(OE)

    elif type == 'pools':
        mdate = tree.attrib['date']
        for meeting in tree.findall('meeting'):
            mnumber = meeting.find('number').text
            for race in meeting.findall('races/race'):
                rnumber = race.find('number').text
                ###Locating the parent Race
                for item in results_l:
                    if isinstance(item, ScheduleRace):
                        if item.number == rnumber:
                            if item.parentMeeting.number == mnumber:
                                if item.parentMeeting.date == mdate:
                                    RA = item
                                    break

                for pool in race.findall('pools/pool'):
                    PO = PoolsPool()
                    PO.set(pool, RA)
                    results_l.append(PO)
                    for leg in pool.findall('legs/leg'):
                        LE = PoolsLeg()
                        LE.set(leg, PO)
                        results_l.append(LE)
                    for entry in pool.findall('entries/entry'):
                        EN = PoolsEntry()
                        EN.set(entry, PO)
                        results_l.append(EN)


def set_sqlid(OBJ):
    OBJ.sqlid = DB.cursor.lastrowid


def write_to_database(OBJ, table):
    if table == 'meetings':
        insert = 'insert into meetings' \
                 '(betslip_type, code, country, date, name, number, nz,penetrometer, status, track_dir, type, venue)' \
                 'values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s );'
        values = (
        OBJ.betslip_type, OBJ.code, OBJ.country, OBJ.date, OBJ.name, OBJ.number, OBJ.nz, OBJ.penetrometer, OBJ.status,
        OBJ.track_dir, OBJ.type, OBJ.venue)

        logging.debug( insert)
        logging.debug( values)
        if DB.query(insert, values) == 0:
            set_sqlid(OBJ)

    if table == 'races':
        insert = 'insert into races' \
                 '(winnerstrainer, winnersbreeding, winnersowner , winnerstime, number, class, length, name, norm_time, overseas_number, stake, status, track, venue, weather,result_status,meeting_id)' \
                 'values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s ,%s, %s, %s, %s,%s ,%s );'
        values = (
        OBJ.winnerstrainer, OBJ.winnersbreeding, OBJ.winnersowner, OBJ.winnerstime, OBJ.parentScheduleRace.number,
        OBJ.parentScheduleRace.classs,
        OBJ.parentScheduleRace.length, OBJ.parentScheduleRace.name, OBJ.parentScheduleRace.norm_time,
        OBJ.parentScheduleRace.overseas_number, OBJ.stake, OBJ.parentScheduleRace.status, OBJ.parentScheduleRace.track,
        OBJ.parentScheduleRace.venue,
        OBJ.parentScheduleRace.weather, OBJ.status, OBJ.parentScheduleRace.parentMeeting.sqlid)

        logging.debug( insert)
        logging.debug( values)
        if DB.query(insert, values) == 0:
            set_sqlid(OBJ)
            if OBJ.parentScheduleRace.sqlid is None:
                set_sqlid(OBJ.parentScheduleRace)

    if table == 'race_options':
        insert = 'insert into race_options' \
                 '(number,type,race_id)' \
                 'values (%s, %s, %s" );'
        values = (OBJ.number, OBJ.type, OBJ.parentRace.sqlid)

        logging.debug( insert)
        logging.debug( values)
        if DB.query(insert, values) == 0:
            set_sqlid(OBJ)

    if table == 'race_entries':
        rank = None
        distance = None
        favouritism = None
        margin = None
        scr = None
        win = None
        plc = None

        if 'ResultPlacing' in OBJ:
            rank = OBJ['ResultPlacing'].rank
            distance = OBJ['ResultPlacing'].distance
            favouritism = OBJ['ResultPlacing'].favouritism
            margin = OBJ['ResultPlacing'].margin

        if 'OddsEntry' in OBJ:
            scr = OBJ['OddsEntry'].scr
            win = OBJ['OddsEntry'].win
            plc = OBJ['OddsEntry'].plc

        if rank is None and 'ResultRunner' in OBJ:
            rank = OBJ['ResultRunner'].finish_position

        if distance is None and 'ResultRunner' in OBJ:
            distance = OBJ['ResultRunner'].distance

        insert = 'insert into race_entries (race_id,barrier, jockey, jockey_allowance, jockey_weight, name, number, scratched, scr, win,plc, rank,favouritism,margin,distance) ' \
                 'values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);'
        values = (OBJ['ScheduleEntry'].parentRace.sqlid, OBJ['ScheduleEntry'].barrier, OBJ['ScheduleEntry'].jockey,
                  OBJ['ScheduleEntry'].jockey_allowance, OBJ['ScheduleEntry'].jockey_weight, OBJ['ScheduleEntry'].name,
                  OBJ['ScheduleEntry'].number,
                  OBJ['ScheduleEntry'].scratched, scr, win, plc, rank, favouritism, margin, distance)

        logging.debug( insert)
        logging.debug( values)
        DB.query(insert, values)

    if table == 'pool_odds':
        insert = 'insert into pool_odds' \
                 '(race_id, bet_type, comingled, comingled_info, comingled_brand)' \
                 'values (%s, %s, %s, %s, %s );'
        values = (OBJ.parentOddsRace.parentScheduleRace.sqlid, OBJ.bet_type, OBJ.comingled, OBJ.comingled_info,
                  OBJ.comingled_brand)

        logging.debug( insert)
        logging.debug( values)
        if DB.query(insert, values) == 0:
            set_sqlid(OBJ)

    if table == 'pools':
        insert = 'insert into pools' \
                 '(race_id, type, number, name, amount)' \
                 'values (%s, %s, %s, %s, %s );'
        values = (OBJ.parentResultRace.parentScheduleRace.sqlid, OBJ.type, OBJ.number, OBJ.name, OBJ.amount)

        logging.debug( insert)
        logging.debug( values)
        if DB.query(insert, values) == 0:
            set_sqlid(OBJ)

    if table == 'pools_pool':
        insert = 'insert into pools_pool (bet_type, total, commingled, commingled_brand, commingled_info, meeting_id, race_id ) ' \
                 'values (%s, %s, %s, %s, %s, %s, %s );'
        values = (OBJ.bet_type, OBJ.total, OBJ.commingled, OBJ.commingled_brand, OBJ.commingled_info,
                  OBJ.parentRace.parentMeeting.sqlid, OBJ.parentRace.sqlid)

        logging.debug( insert)
        logging.debug( values)
        if DB.query(insert, values) == 0:
            set_sqlid(OBJ)

    if table == 'pools_pool_entries':
        insert = 'insert into pools_pool_entries (number, scratched, value, odds, favouritism, pool_id ) ' \
                 ' values   (%s, %s, %s, %s, %s, %s );'
        values = (OBJ.number, OBJ.scratched, OBJ.value, OBJ.odds, OBJ.favouritism, OBJ.parentPool.sqlid)

        logging.debug( insert)
        logging.debug( values)
        DB.query(insert, values)

    if table == 'pools_pool_legs':
        insert = 'insert into pools_pool_legs (number, meet, race, pool_id ) ' \
                 'values  (%s, %s, %s, %s );'
        values = (OBJ.number, OBJ.meet, OBJ.race, OBJ.parentPool.sqlid)

        logging.debug( insert)
        logging.debug( values)
        DB.query(insert, values)

    return 0


def write_data():
    for OBJ in results_l:
        if isinstance(OBJ, ScheduleMeeting):
            write_to_database(OBJ, 'meetings')

    for OBJ in results_l:
        if isinstance(OBJ, ResultRace):
            write_to_database(OBJ, 'races')

    for OBJ in results_l:
        if isinstance(OBJ, ScheduleOptions):
            write_to_database(OBJ, 'race_options')

    for OBJ in results_l:
        if isinstance(OBJ, ScheduleEntry):
            data = dict()
            data['ScheduleEntry'] = OBJ
            for OB2 in results_l:
                if isinstance(OB2, OddsEntry):
                    # if belongs to same race and have entry number equal
                    if OB2.parentOddsRace.parentScheduleRace.sqlid == OBJ.parentRace.sqlid and OB2.number == OBJ.number:
                        data['OddsEntry'] = OB2
                elif isinstance(OB2, ResultPlacing):
                    if OB2.parentResultRace.parentScheduleRace.sqlid == OBJ.parentRace.sqlid and OB2.number == OBJ.number:
                        data['ResultPlacing'] = OB2
                elif isinstance(OB2, ResultRunner):
                    if OB2.parentResultRace.parentScheduleRace.sqlid == OBJ.parentRace.sqlid and OB2.number == OBJ.number:
                        data['ResultRunner'] = OB2

                if len(data) == 4:
                    break

            write_to_database(data, 'race_entries')

    for OBJ in results_l:
        if isinstance(OBJ, OddsBetType):
            write_to_database(OBJ, 'pool_odds')

    for OBJ in results_l:
        if isinstance(OBJ, ResultPool):
            write_to_database(OBJ, 'pools')

    for OBJ in results_l:
        if isinstance(OBJ, PoolsPool):
            write_to_database(OBJ, 'pools_pool')

    for OBJ in results_l:
        if isinstance(OBJ, PoolsEntry):
            write_to_database(OBJ, 'pools_pool_entries')

    for OBJ in results_l:
        if isinstance(OBJ, PoolsLeg):
            write_to_database(OBJ, 'pools_pool_legs')

    DB.commit()
    DB.disconnect()
    return 0


def begin():
    logging.info( "Downloading XML " + url_schedule)
    tree = open_xml_url(url_schedule)
    logging.info( "Parsing XML file")
    parse_xml(tree, 'schedule')
    logging.info("Downloading XML " + url_results)
    tree = open_xml_url(url_results)
    logging.info( "Parsing XML file")
    parse_xml(tree, 'results')
    logging.info( "Downloading XML " + url_odds)
    tree = open_xml_url(url_odds)
    logging.info( "Parsing XML file")
    parse_xml(tree, 'odds')
    logging.info( "Downloading XML " + url_pools)
    tree = open_xml_url(url_pools)
    logging.info( "Parsing XML file")
    parse_xml(tree, 'pools')

    logging.info( "Doing database operations")
    write_data()


logger = logging.getLogger()

if loglevel == 'debug':
    logging.basicConfig(level=logging.DEBUG)
else:
    logging.basicConfig(level=logging.INFO)

logging.info("Version 0.6")
DB = Db()

if __name__ == "__main__":
    begin()


def handler(event, context):
    begin()
